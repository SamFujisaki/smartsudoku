package fr.arkhist.smartsudoku;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    AlertDialog overwriteDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button playButton = this.findViewById(R.id.playButton);
        playButton.setOnClickListener(view -> {
                tryStartGame();
        });

        Button scoreButton = this.findViewById(R.id.scoreButton);
        scoreButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), StatsActivity.class);
            startActivity(intent);
        });

        Button continueButton = this.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(view -> {
            continueGame();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.overwrite_query)
                .setTitle(R.string.save_exists);

        builder.setPositiveButton(R.string.yes, (dialog, id) -> {
            startGame(true);
            this.findViewById(R.id.regularMenu).setVisibility(View.GONE);
        });
        builder.setNegativeButton(R.string.no, (dialog, id) -> {
            startGame(false);
            this.findViewById(R.id.regularMenu).setVisibility(View.GONE);
        });
        overwriteDialog = builder.create();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.findViewById(R.id.regularMenu).setVisibility(View.VISIBLE);
    }

    private void tryStartGame() {
        if (SudokuActivity.saveExists(this))
            overwriteDialog.show();
        else
            startGame(false);
    }

    private void startGame(boolean isContinuing) {
        Intent intent = new Intent(getApplicationContext(), isContinuing ?
                SudokuActivity.class : SelectActivity.class);
        startActivity(intent);
    }

    private void continueGame() {
        if (SudokuActivity.saveExists(this)) {
            startGame(true);
        } else {
            Toast.makeText(getApplicationContext(), R.string.save_not_exist, Toast.LENGTH_SHORT).show();
        }
    }
}
