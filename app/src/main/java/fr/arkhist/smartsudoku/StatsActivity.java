package fr.arkhist.smartsudoku;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;


public class StatsActivity extends AppCompatActivity {

    public static final String SAVE_SCORE = "scoreSave";

    TextView scores[] = new TextView[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        scores[0] = this.findViewById(R.id.easy_score);
        scores[1] = this.findViewById(R.id.medium_score);
        scores[2] = this.findViewById(R.id.hard_score);

        if(!scoreExists(this)) {
            initScore(this);
        }
        else {
            loadScoreData();
        }
    }

    private void loadScoreData() {
        try (FileInputStream scoreStream = openFileInput(SAVE_SCORE)){
            byte[] byteArray = new byte[24];
            scoreStream.read(byteArray, 0, 24);
            scoreStream.close();
            this.deserialize(ByteBuffer.wrap(byteArray));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("DefaultLocale")
    private void deserialize(ByteBuffer scoreBuffer) {
        for(int i = 0; i < 3; i++) {
            long score = scoreBuffer.getLong();
            if(score < 0)
                scores[i].setText("--:--");
            else
                scores[i].setText(String.format("%02d:%02d", (score / 1000) / 60, (score / 1000) % 60));
        }
    }

    public static boolean scoreExists(AppCompatActivity activity) {
        File save = new File(activity.getApplicationContext().getFilesDir(), SAVE_SCORE);
        return save.exists();
    }

    private static void initScore(AppCompatActivity activity) {
        ByteBuffer saveBuffer = ByteBuffer.allocate(24);
        long easy = -1;
        long medium = -2;
        long hard = -3;

        saveBuffer.putLong(easy);
        saveBuffer.putLong(medium);
        saveBuffer.putLong(hard);
        saveBuffer.flip();

        try (FileOutputStream file = activity.openFileOutput(SAVE_SCORE, MODE_PRIVATE)) {
            file.getChannel().write(saveBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void updateScore(SudokuActivity activity, int saveIndex, long inScore) {
        if(!scoreExists(activity))
            initScore(activity);

        ByteBuffer buffer;

        try (FileInputStream scoreStream = activity.openFileInput(SAVE_SCORE)) {
            byte byteArray[] = new byte[24];
            scoreStream.read(byteArray, 0, 24);
            scoreStream.close();
            buffer = ByteBuffer.wrap(byteArray);
            updateBuffer(buffer, saveIndex, inScore);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try (FileOutputStream scoreStream = activity.openFileOutput(SAVE_SCORE, MODE_PRIVATE)) {
            scoreStream.getChannel().write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void updateBuffer(ByteBuffer scoreBuffer, int saveIndex, long inScore) {
        long score[] = new long[3];
        for(int i =0; i < 3; i++)
            score[i] = scoreBuffer.getLong();
        score[saveIndex] = scoreCompare(score[saveIndex], inScore);
        scoreBuffer.flip();
        for (int i = 0; i < 3; i++)
            scoreBuffer.putLong(score[i]);
        scoreBuffer.flip();
    }

    private static long scoreCompare(long a, long b) {
        if (a <= -1)
            return b;
        return Math.min(a, b);
    }

}
