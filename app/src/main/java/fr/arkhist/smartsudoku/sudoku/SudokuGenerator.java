package fr.arkhist.smartsudoku.sudoku;

import fr.arkhist.smartsudoku.SudokuActivity;
import fr.arkhist.smartsudoku.util.Util;

public class SudokuGenerator {

    public static byte[][] generateRandomSudoku(SudokuActivity.SudokuDifficulty currentDifficulty) {
        byte[][] solution = new byte[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}
        };
        solution = SudokuSolver.solve(solution, true);

        int[] holes = new int[81];
        for(int i = 0; i < 81; i++)
            holes[i] = i;
        Util.shuffleArray(holes);

        for(int i = 0; i < Math.floor(81*currentDifficulty.triesPercent); i++) {
            int y = holes[i]/9;
            int x = holes[i]%9;
            int pr = solution[y][x];
            solution[y][x] = (byte)0;
            if(SudokuSolver.solvePrevent(solution, true, y, x, pr) == null) {
                continue;
            }
            else {
                solution[y][x] = (byte)pr;
            }
        }

        return solution;
    }
}
