package fr.arkhist.smartsudoku.sudoku;

import fr.arkhist.smartsudoku.util.Util;

public class SudokuGrader {

    interface Technique {
        int apply(byte[][] sudoku, int[][][] possibles);
    }

    private static final Technique[] techniques = new Technique[] {
        SudokuGrader::techniqueSingleCandidate,
        SudokuGrader::techniqueSinglePosition
    };

    private static int leftToApply = 0;

    static void applyValue(byte[][] si, int[][][] possible, int y, int x, int val) {
        si[y][x] = (byte)val;
        for(int yp = 0; yp < 9; yp++)
            possible[yp][x][val-1] = 0;
        for(int xp = 0; xp < 9; xp++)
            possible[y][xp][val-1] = 0;
        int ys = y/3;
        int xs = x/3;
        for(int yp = ys; yp < ys+3; yp++)
            for(int xp = xs; xp < xs+3; xp++)
                possible[yp][xp][val-1] = 0;
        leftToApply--;
    }

    public static int techniqueSingleCandidate(byte[][] si, int[][][] possibles) {
        for(int y = 0; y < 9; y++)
            for(int x = 0; x < 9; x++)
                if(applySingleCandidate(si, possibles, y, x) != -1)
                    return 0;
        return -1;
    }

    public static int applySingleCandidate(byte[][] si, int[][][] possible, int y, int x) {
        if(Util.countNonZero(possible[y][x]) == 1) {
            int val = Util.zeroArrPop(possible[y][x]);
            applyValue(si, possible, y, x, val);
            return 0;
        }
        return -1;
    }

    public static int techniqueSinglePosition(byte[][] si, int[][][] possibles) {
        for(int y = 0; y < 9; y++)
            for(int x = 0; x < 9; x++)
                if(applySinglePosition(si, possibles, y, x) != -1)
                    return 1;
        return -1;
    }

    public static int applySinglePosition(byte[][] si, int[][][] possibles, int y, int x) {
        // Test Y Line
        {
            int[] possible = SudokuSolver.getPossible(si, y, x);
            int[] possible2;
            for (int yp = 0; yp < 9; yp++) {
                if (yp == y) continue;
                possible2 = SudokuSolver.getPossible(si, yp, x);
                int value;
                while ((value = Util.zeroArrPop(possible2)) != 0) {
                    for (int i = 0; i < possible.length; i++) {
                        if (possible[i] == value) {
                            possible[i] = 0;
                            break;
                        }
                    }
                }
            }
            if (Util.countNonZero(possible) == 1) {
                int val = Util.zeroArrPop(possible);
                applyValue(si, possibles, y, x, val);
                return 1;
            }
        }
        // Test X Line
        {
            int[] possible = SudokuSolver.getPossible(si, y, x);
            int[] possible2;
            for (int xp = 0; xp < 9; xp++) {
                if (xp == x) continue;
                possible2 = SudokuSolver.getPossible(si, y, xp);
                int value;
                while ((value = Util.zeroArrPop(possible2)) != 0) {
                    for (int i = 0; i < possible.length; i++) {
                        if (possible[i] == value) {
                            possible[i] = 0;
                            break;
                        }
                    }
                }
            }
            if (Util.countNonZero(possible) == 1) {
                int val = Util.zeroArrPop(possible);
                applyValue(si, possibles, y, x, val);
                return 1;
            }
        }
        // Test Square
        {
            int[] possible = SudokuSolver.getPossible(si, y, x);
            int[] possible2;
            int ys = y / 3;
            int xs = x / 3;
            for (int yp = ys; yp < ys + 3; yp++) {
                for (int xp = xs; xp < xs + 3; xp++) {
                    if (xp == x && yp == y) continue;
                    possible2 = SudokuSolver.getPossible(si, yp, xp);
                    int value;
                    while ((value = Util.zeroArrPop(possible2)) != 0) {
                        for (int i = 0; i < possible.length; i++) {
                            if (possible[i] == value) {
                                possible[i] = 0;
                                break;
                            }
                        }
                    }
                }
            }
            if (Util.countNonZero(possible) == 1) {
                int val = Util.zeroArrPop(possible);
                applyValue(si, possibles, y, x, val);
                return 1;
            }
        }
        return -1;
    }

    public static int solveHeuristic(byte[][] si, byte[][] solution) {
        int difficulty = 0;
        byte[][] sudoku = new byte[9][9];
        int[][][] possibles = new int[9][9][9];
        // Loading the sudoku data
        {
            leftToApply = 0;
        for(int y = 0; y < 9; y++)
            for(int x = 0; x < 9; x++) {
                sudoku[y][x] = si[y][x];
                if(si[y][x] == 0)
                    leftToApply++;
            }
        for(int y = 0; y < 9; y++)
            for(int x = 0; x < 9; x++)
                possibles[y][x] = SudokuSolver.getPossible(si, y, x);
        }

        while(leftToApply > 0)  {
            boolean used = false;
            for(int i = 0; i < techniques.length; i++) {
                int ret = techniques[i].apply(sudoku, possibles);
                if (ret == -1)
                    continue;
                difficulty += ret;
                used = true;
                break;
            }
            if (used)
                continue;
            boolean found = false;
            for(int i = 2; i < 10; i++) {
                for(int y = 0; y < 9; y++) {
                    for (int x = 0; x < 9; x++) {
                        if (Util.countNonZero(possibles[y][x]) == i) {
                            difficulty += (int) (Math.pow(4, i));
                            applyValue(sudoku, possibles, y, x, solution[y][x]);
                            found = true;
                            break;
                        }
                    }
                    if(found)
                        break;
                }
                if(found)
                    break;
            }
            if(!found) {
                System.out.println("Something weird happened.");
            }
        }


        return difficulty;
    }
}
