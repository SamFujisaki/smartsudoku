package fr.arkhist.smartsudoku.sudoku;

import java.util.ArrayDeque;
import java.util.Deque;

import fr.arkhist.smartsudoku.util.Util;

public class SudokuSolver {

    public static class SolveStep {
        public final int y;
        public final int x;
        public final int[] possibles;

        public SolveStep(int y, int x, int[] possibles) {
            this.y = y;
            this.x = x;
            this.possibles = possibles;
        }
    }

    public static int[] getPossiblePrevent(byte[][] sudoku, int y, int x, int prevent) {
        int[] possible = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        if(prevent != 0)
            possible[prevent-1] = 0;
        for (int i = 0; i < 9; i++) {
            if (sudoku[y][i] != 0) {
                possible[sudoku[y][i] - 1] = 0;
            }
            if (sudoku[i][x] != 0) {
                possible[sudoku[i][x] - 1] = 0;
            }
        }
        int squareY = y / 3;
        int squareX = x / 3;
        for (int ys = squareY * 3; ys < squareY * 3 + 3; ys++)
            for (int xs = squareX * 3; xs < squareX * 3 + 3; xs++)
                if (sudoku[ys][xs] != 0)
                    possible[sudoku[ys][xs] - 1] = 0;

        return possible;
    }

    public static int[] getPossible(byte[][] sudoku, int y, int x) {
        return getPossiblePrevent(sudoku, y, x, 0);
    }

    public static byte[][] solvePrevent(byte[][] si, boolean randomChoice, int yp, int xp, int prevent) {
        byte[][] sudoku = new byte[9][9];
        for(int y = 0; y < 9; y++)
            for(int x = 0; x < 9; x++)
                sudoku[y][x] = si[y][x];

        Deque<SolveStep> stepStack = new ArrayDeque<>();
        int[] possible = null;

        int y = 0;
        int x = 0;
        while (y < 9) {
            if (sudoku[y][x] == 0) {
                if (possible == null) {
                    if(yp == y && xp == x)
                        possible = SudokuSolver.getPossiblePrevent(sudoku, y, x, prevent);
                    else
                        possible = SudokuSolver.getPossible(sudoku, y, x);
                    if (randomChoice) {
                        Util.shuffleArray(possible);
                    }
                }
                if (Util.isZero(possible)) {
                    if (stepStack.size() == 0)
                        return null;
                    SolveStep step = stepStack.pop();
                    y = step.y;
                    x = step.x;
                    possible = step.possibles;
                    sudoku[y][x] = 0;
                    continue;
                } else {
                    int value = Util.zeroArrPop(possible);
                    sudoku[y][x] = (byte) value;
                    stepStack.push(new SolveStep(y, x, possible));
                    possible = null;
                }
            }

            x = (x + 1) % 9;
            if (x == 0)
                y++;
        }

        return sudoku;
    }

    public static byte[][] solve(byte[][] sudokuInput, boolean randomChoice) {
        return solvePrevent(sudokuInput, randomChoice, -1, -1, 0);
    }

}
